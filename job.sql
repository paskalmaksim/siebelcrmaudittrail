/*every 15 minutes*/
begin
  import_s_audit_item(date_form => trunc(sys_extract_utc(systimestamp), 'mi') - 15 * 1 / 24 / 60,
                      date_to   => trunc(sys_extract_utc(systimestamp), 'mi'));
end;