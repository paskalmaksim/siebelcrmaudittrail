CREATE OR REPLACE PROCEDURE import_s_audit_item (date_form    DATE,
                                                 date_to      DATE)
   AUTHID DEFINER
IS
   var_str         CLOB;
   var_str_len     NUMBER;
   i               NUMBER;
   list_len        NUMBER;
   block_name      CHAR (1);
   NEXT_BLOCK      NUMBER;
   array_len       NUMBER;
   last_len        NUMBER;
   next_len        NUMBER;

   ast_pos         NUMBER;

   CURSOR c_audit_items
   IS
      SELECT *
        FROM siebel.s_audit_item i
       WHERE     i.audit_log IS NOT NULL
             AND i.operation_dt >= date_form
             AND i.operation_dt < date_to;

   var_value       VARCHAR2 (2000);
   var_rvalue      VARCHAR2 (2000);

   TYPE T_REC IS RECORD
   (
      C_NAME   VARCHAR2 (2000),
      C_NEW    VARCHAR2 (2000),
      C_OLD    VARCHAR2 (2000)
   );

   TYPE T_ARRAY IS TABLE OF T_REC;

   C_BLOCK         T_ARRAY;
   C_BLOCK_INDEX   NUMBER;

   J_BLOCK         T_ARRAY;
   J_BLOCK_INDEX   NUMBER;
BEGIN
   FOR curs IN c_audit_items
   LOOP
      var_str := curs.audit_log;
      var_str_len := LENGTH (var_str);
      i := 1;
      next_len := 1;
      last_len := 1;
      list_len := 1;
      NEXT_BLOCK := 2;
      C_BLOCK_INDEX := 0;
      J_BLOCK_INDEX := 0;

      C_BLOCK := T_ARRAY ();
      J_BLOCK := T_ARRAY ();

      WHILE i <= var_str_len
      LOOP
         ast_pos := INSTR (var_str, '*', i + next_len);

         IF ast_pos = 0
         THEN
            ast_pos := var_str_len + 1;
         END IF;

         var_value := SUBSTR (var_str, i, ast_pos - i);

         IF i = 1
         THEN
            var_rvalue := var_value;
            next_len := var_value;
         ELSE
            var_rvalue := SUBSTR (var_value, 1, last_len);
            next_len := SUBSTR (var_value, last_len + 1);
         END IF;

         IF list_len = NEXT_BLOCK
         THEN
            block_name := SUBSTR (var_rvalue, 1, 1);
            array_len := SUBSTR (var_rvalue, 2);
            NEXT_BLOCK := list_len + array_len + 1;

            C_BLOCK_INDEX := 0;
            J_BLOCK_INDEX := 0;

            IF block_name = 'C'
            THEN
               C_BLOCK.EXTEND (array_len);
            END IF;

            IF block_name = 'J'
            THEN
               J_BLOCK.EXTEND (array_len);
            END IF;
         END IF;

         /*C BLOCK*/
         IF block_name = 'C' AND C_BLOCK_INDEX > 0
         THEN
            C_BLOCK (C_BLOCK_INDEX).C_NAME := var_rvalue;
         END IF;

         IF block_name = 'N' AND C_BLOCK_INDEX > 0
         THEN
            C_BLOCK (C_BLOCK_INDEX).C_NEW := var_rvalue;
         END IF;

         IF block_name = 'O' AND C_BLOCK_INDEX > 0
         THEN
            C_BLOCK (C_BLOCK_INDEX).C_OLD := var_rvalue;
         END IF;

         /*J BLOCK*/
         IF block_name = 'J' AND J_BLOCK_INDEX > 0
         THEN
            J_BLOCK (C_BLOCK_INDEX).C_NAME := var_rvalue;
         END IF;

         IF block_name = 'L' AND J_BLOCK_INDEX > 0
         THEN
            J_BLOCK (C_BLOCK_INDEX).C_NEW := var_rvalue;
         END IF;

         IF block_name = 'K' AND J_BLOCK_INDEX > 0
         THEN
            J_BLOCK (C_BLOCK_INDEX).C_OLD := var_rvalue;
         END IF;

         i := i + LENGTH (var_value) + 1;
         list_len := list_len + 1;
         C_BLOCK_INDEX := C_BLOCK_INDEX + 1;
         J_BLOCK_INDEX := J_BLOCK_INDEX + 1;
         last_len := next_len;
      END LOOP;

      IF C_BLOCK.COUNT () > 0
      THEN
         FOR i IN C_BLOCK.FIRST .. C_BLOCK.LAST
         LOOP
            INSERT INTO s_audit_item (audit_row_id,
                                            buscomp_name,
                                            field_name,
                                            operation_cd,
                                            record_id,
                                            user_id,
                                            operation_dt,
                                            child_bc_name,
                                            new_val,
                                            old_val)
                 VALUES (curs.ROW_ID,
                         curs.BUSCOMP_NAME,
                         C_BLOCK (i).C_NAME,
                         curs.operation_cd,
                         curs.RECORD_ID,
                         curs.USER_ID,
                         curs.OPERATION_DT,
                         curs.CHILD_BC_NAME,
                         C_BLOCK (i).C_NEW,
                         C_BLOCK (i).C_OLD);
         END LOOP;

         COMMIT;
      END IF;

      IF J_BLOCK.COUNT () > 0
      THEN
         FOR i IN J_BLOCK.FIRST .. J_BLOCK.LAST
         LOOP
            INSERT INTO s_audit_item (audit_row_id,
                                            buscomp_name,
                                            field_name,
                                            operation_cd,
                                            record_id,
                                            user_id,
                                            operation_dt,
                                            child_bc_name,
                                            new_val,
                                            old_val)
                 VALUES (curs.ROW_ID,
                         curs.BUSCOMP_NAME,
                         J_BLOCK (i).C_NAME,
                         curs.operation_cd,
                         curs.RECORD_ID,
                         curs.USER_ID,
                         curs.OPERATION_DT,
                         curs.CHILD_BC_NAME,
                         J_BLOCK (i).C_NEW,
                         J_BLOCK (i).C_OLD);
         END LOOP;

         COMMIT;
      END IF;

      C_BLOCK.DELETE ();
      J_BLOCK.DELETE ();
   END LOOP;
END;
/